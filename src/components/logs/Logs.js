import React, { useEffect } from "react";
import { getLogs } from "../../actions/logActions";
import LogItem from "./LogItem";
import PropTypes from "prop-types";

import { connect } from "react-redux";
import Preloader from "../layout/Preloader";

const Logs = ({ log: { logs, loading }, getLogs }) => {
  useEffect(() => {
    getLogs();
    // eslint-disable-next-line
  }, []);

  if (loading || logs === null) {
    return <Preloader />;
  }

  return (
    <div>
      <ul className="collection with-header">
        <li className="collection-header">
          <h4 className="center">System Logs</h4>
        </li>
        {!loading && logs.length === 0 ? (
          <p className="center">No logs to show...</p>
        ) : (
          logs.map((log) => <LogItem key={log.id} log={log} />)
        )}
      </ul>
    </div>
  );
};

Logs.propTypes = {
  log: PropTypes.objectOf.isRequired,
  getLogs: PropTypes.func.isRequired,
};

const mapStateToPtops = (state) => ({
  log: state.log,
});

export default connect(mapStateToPtops, { getLogs })(Logs);
